===============================================
Python Interface To Tascam Firewire Audio Units
===============================================

Introduction
============

This Python module provides an interface into the libhinawa library
for accessing and controlling select Tascam Firewire audio units.
The units supported are:

 * Tascam FW-1884
 * Tascam FW-1804
 * Tascam FW-1802

The current maintainer of this project has access to an FW-1884 unit
and that has the most support and testing so far.

The code was originally written by Takashi Sakamoto as part of the
hinawa-utils package [1]_. Since the original maintainer announced he would no
longer be maintaining the hinawa-utils code, I have broken it out into
a generic python module.

The modules include applications of below specifications:

 * IEEE 1212:2001 - IEEE Standard for a Control and Status Registers (CSR)
   Architecture for Microcomputer Buses
 * IEEE 1394:2008 - IEEE Standard for a High-Performance Serial Bus
 * AV/C Digital Interface Command Set General Specification Version 4.2
   (Sep. 2004, 1394 Trade Association)
 * AV/C Audio Subunit Specification 1.0 (Oct. 2000, 1394 Trade Association)
 * AV/C Connection and Compatibility Management Specification 1.1
   (Mar. 2003, 1394 Trade Association)
 * Configuration ROM for AV/C Devices 1.0 (Dec. 2000, 1394 Trade Association)
 * AV/C Stream Format Information Specification 1.1 - Working draft
   revision 0.5 (Apr. 2005, 1394 Trade Association)
 * Vendor specific protocols:
    * Protocol for FireWire series of TEAC (TASCAM).

Requirements
============

 * Python 3.4 or later
    * https://docs.python.org/3/library/enum.html
    * https://docs.python.org/3/library/pathlib.html
 * PyGObject
    * https://gitlab.gnome.org/GNOME/pygobject
 * libhinawa 2.0.0 or later, with gir support
    * https://github.com/takaswie/libhinawa
 * Python ieee1394_config_rom module
    * https://gitlab.com/tascam-fw-1884/ieee1394-config-rom


Examples
========

Examples for using the Python modules included in this package can be
found in the `examples` directory.

License
=======

 * All modules are licensed under GNU Lesser General Public License version 3 or
   later.
 * All scripts are licensed under GNU General Public License version 3 or later.


.. [1] https://github.com/takaswie/hinawa-utils
